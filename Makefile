# /*
# * Copyright (C) 2019 ChinhPC <chinhphancong@outlook.com>
# *
# * Author: ChinhPC
# *
# * This file is free software: you can redistribute it and/or modify it
# * under the terms of the GNU General Public License as published by the
# * Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.
# *
# * This file is distributed in the hope that it will be useful, but
# * WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# * See the GNU General Public License for more details.
# *
# * You should have received a copy of the GNU General Public License along
# * with this program.  If not, see <http://www.gnu.org/licenses/>.
# */

MODULE_NAME := ws2812b
OBJ_SRCS :=  ws2812b-spi.o
$(MODULE_NAME)-y := $(OBJ_SRCS)
obj-m := $(MODULE_NAME).o

KDIR := /tftpboot/nfs/opifs/lib/modules/4.19.63/build #/lib/modules/$(shell uname -r)/build
PWD := $(shell pwd)
INSTALL_MOD_PATH := INSTALL_MOD_PATH=/tftpboot/nfs/opifs
ARCH := ARCH=arm
CROSS_COMPILE := CROSS_COMPILE=arm-linux-gnueabihf-
defconfig := sunxi_defconfig

all: app
#	$(MAKE) -C $(KDIR) $(ARCH) $(CROSS_COMPILE) $(XTRA) $(defconfig)
	$(MAKE) -C $(KDIR) $(ARCH) $(CROSS_COMPILE) $(XTRA) modules_prepare
	$(MAKE) -C $(KDIR) $(ARCH) $(CROSS_COMPILE) $(XTRA) M=$(PWD) modules

clean:
	$(MAKE) -C $(KDIR) M=$(PWD) clean
	rm app.out test

install:
	$(MAKE) all	#$(KDIR) $(ARCH) $(CROSS_COMPILE) $(XTRA) M=$(PWD) modules_prepare modules
	sudo $(MAKE) -C $(KDIR) $(ARCH) $(CROSS_COMPILE) $(XTRA) M=$(PWD) $(INSTALL_MOD_PATH) modules_install
	sudo cp app.out /tftpboot/nfs/opifs/home/root
	sync

app: user_app.c
	arm-linux-gnueabihf-gcc -o app.out user_app.c

test: test.c
	gcc -o test test.c
	./test