 /*
 * Copyright (C) 2019 ChinhPC <chinhphancong@outlook.com>
 *
 * Author: ChinhPC
 *
 * This file is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#include <linux/init.h>
#include <linux/module.h>
#include <linux/spi/spi.h>
#include <linux/cdev.h>
#include <linux/string.h>
#include <linux/uaccess.h>
#include "ws2812b.h"

//#define SPIDEV_MAJOR			153	/* assigned */
//#define N_SPI_MINORS			32	/* ... up to 256 */
//#define MY_BUS_NUM 1
#define N_MINORS 1

#define DEVICE_NAME "ws2812b-spi"

#define DEBUG
#ifdef DEBUG
void print_rgbled_pixel(struct rgbled_pixel data)
{
	printk("%hhX\t", data.red_led); printk("%hhX\t", data.green_led); printk("%hhX", data.blue_led);
}

void print_spiled_pixel(struct spi_singleled data)
{
	printk("%hhX\t", data.data[2]); printk("%hhX\t", data.data[1]); printk("%hhX", data.data[0]);
}
#endif

//struct cdev my_cdev;
struct class *my_class;
dev_t my_dev_num;

static int     my_open( struct inode *, struct file * );
static ssize_t my_read( struct file * ,        char *  , size_t, loff_t *);
static ssize_t my_write(struct file * , const  char *  , size_t, loff_t *);
static int     my_close(struct inode *, struct file * );

struct file_operations my_fops = {
        read    :       my_read,
        write   :       my_write,
        open    :       my_open,
        release :       my_close,
        owner   :       THIS_MODULE
};

struct ws2812b_platdata{
struct spi_device *pspidev;
struct cdev my_cdev;
struct spiled_strip *spiled_strip;
struct rgbled_strip *rgbled_strip;
} ws2812b_platdata;

struct spi_singleled rgb_to_spi_singleled(unsigned char *led)
{
	int i;
	unsigned int tmp = 0;
	struct spi_singleled tmp_spi_singleled = {{0, 0, 0}};

	for(i = 0; i < 8; i++)
	{
		if((*led >> i) & 0x1) tmp |= (0x3 << (3 * i));
		else tmp |= (0x1 << (3 * i));
	}

	memcpy(&tmp_spi_singleled, &tmp, 3);

	return tmp_spi_singleled;
}

static int decode_rgb_to_spi(struct rgbled_strip *rgbled_strip, struct spiled_strip *spiled_strip)
{
	int i;
	struct spi_singleled tmp_spi_singleled = {{0, 0, 0}};

	for(i=0; i<12; i++)
	{
	tmp_spi_singleled = rgb_to_spi_singleled(&(rgbled_strip->led1.red_led) + i);
	memcpy(&(spiled_strip->data[0].data[0]) + i*3,(char*)&tmp_spi_singleled,3);
	}
	return 0;
}

/*
 * file operation: OPEN
 * */
static int my_open(struct inode *inod, struct file *fil)
{
	printk("\n*****Some body is opening me*****\n");
	return 0;
}


/*
 * file operation: READ
 * */
static ssize_t my_read(struct file *filp, char *buff, size_t len, loff_t *off)
{
	printk("FILE OPERATION READ\n");
	return 0;
}


/*
 * file operation: WRITE
 * */
static ssize_t my_write(struct file *filp, const char *buff, size_t len, loff_t *off)
{
	unsigned char count = 0;
	int err;
	int i;
#ifdef DEBUG
	struct spiled_strip xample;
	xample = (struct spiled_strip){(struct spi_singleled){0xDB,0x6D,0xB6},(struct spi_singleled){0xDB,0x6D,0xB6},(struct spi_singleled){0xDB,0x6D,0xB6},(struct spi_singleled){0xDB,0x6D,0xB6},(struct spi_singleled){0xDB,0x6D,0xB6},(struct spi_singleled){0xDB,0x6D,0xB6},(struct spi_singleled){0xDB,0x6D,0xB6},(struct spi_singleled){0xDB,0x6D,0xB6},(struct spi_singleled){0xDB,0x6D,0xB6},(struct spi_singleled){0xDB,0x6D,0xB6},(struct spi_singleled){0xDB,0x6D,0xB6},(struct spi_singleled){0xDB,0x6D,0xB6}};
#endif
	// -- copy the string from the user space program which open and write this device
	if (len <= sizeof(*(ws2812b_platdata.rgbled_strip)))
		count = copy_from_user( ws2812b_platdata.rgbled_strip, buff, len );
	else
		count = copy_from_user( ws2812b_platdata.rgbled_strip, buff, sizeof(*(ws2812b_platdata.rgbled_strip)) );

	err = decode_rgb_to_spi(ws2812b_platdata.rgbled_strip, ws2812b_platdata.spiled_strip);
#ifdef DEBUG
	for (i=0; i<12; i++)
		{
			print_spiled_pixel(ws2812b_platdata.spiled_strip->data[i]);
		}
#endif
	spi_write(ws2812b_platdata.pspidev, ws2812b_platdata.spiled_strip, sizeof(struct spiled_strip));

	return count;

}


/*
 * file operation : CLOSE
 * */
static int my_close(struct inode *inod, struct file *fil)
{
	printk("*****Some body is closing me*****\n");
	return 0;
}


static int char_driver_init(struct ws2812b_platdata *pdata)
{
	int err;
	/* Request the kernel for N_MINOR devices */
	alloc_chrdev_region(&my_dev_num, 0, N_MINORS, DEVICE_NAME);
//	register_chrdev_region(SPIDEV_MAJOR, 1, char *name);
	/* Create a class : appears at /sys/class */
	my_class = class_create(THIS_MODULE, "my_driver_class");

//    /* Initialize and create each of the device(cdev) */
//    for (i = 0; i < N_MINORS; i++) {
//		// -- initial the char device
//        /* Associate the cdev with a set of file_operations */
        cdev_init(&pdata->my_cdev, &my_fops);
//        pdata->my_cdev.owner = THIS_MODULE;
        /* Build up the current device number. To be used further */
//        curr_dev = MKDEV(MAJOR(dev_num), MINOR(dev_num) + i);

        /* Create a device node for this device. Look, the class is
         * being used here. The same class is associated with N_MINOR
         * devices. Once the function returns, device nodes will be
         * created as /dev/my_dev0, /dev/my_dev1,... You can also view
         * the devices under /sys/class/my_driver_class.
         */
        device_create(my_class, &pdata->pspidev->dev, my_dev_num, NULL, "my_dev%d", 1);

        /* Now make the device live for the users to access */
		err = cdev_add(&pdata->my_cdev,my_dev_num, 1);
			// -- check error of adding char device
		if (err < 0)
		{
			printk("\n %s: Device Add Error\n",THIS_MODULE->name);
			return -1;
		}
		printk("Hello World. This is my char dev. major= %d, minor= %d \n",MAJOR(my_dev_num),MINOR(my_dev_num));

	return 0;
}

static int ws2812b_probe(struct spi_device *spi)
{
/* allocate our buffer */
	ws2812b_platdata.spiled_strip = devm_kzalloc(&spi->dev, sizeof(*(ws2812b_platdata.spiled_strip)), GFP_KERNEL);
/* allocate our LED data */
	ws2812b_platdata.rgbled_strip = devm_kzalloc(&spi->dev, sizeof(*(ws2812b_platdata.rgbled_strip)), GFP_KERNEL);
	ws2812b_platdata.pspidev = spi;
	return 0;
}

static int ws2812b_remove(struct spi_device *spi)
{

//	cdev_del(&ws2812b_platdata.my_cdev);
	class_destroy (my_class);
	device_destroy(my_class, my_dev_num);
	printk ("Unloading ws2812b_device_driver.\n");
	return 0;
}

/* define the match table */
static const struct of_device_id ws2812b_of_match[] = {
	{
		.compatible	= "worldsemi,ws2812b",
	},
	{ }
};

MODULE_DEVICE_TABLE(of, ws2812b_of_match);

static struct spi_driver ws2812b_driver = {
	.driver = {
		.name = DEVICE_NAME,
		.owner = THIS_MODULE,
		.of_match_table = ws2812b_of_match,
	},
	.probe = ws2812b_probe,
	.remove = ws2812b_remove,
};

//module_spi_driver(ws2812b_driver);

static int __init mydriver_init(void)
{
	int status;
	status = char_driver_init(&ws2812b_platdata);
	if (status < 0)
		return status;
	return spi_register_driver(&ws2812b_driver);
}
module_init(mydriver_init);

static void __exit mydriver_exit(void)
{
	spi_unregister_driver(&ws2812b_driver);
}
module_exit(mydriver_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("ChinhPC <chinhphancong@outlook.com>");
MODULE_DESCRIPTION("WS2812b SPI module example");
