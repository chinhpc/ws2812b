 /*
 * Copyright (C) 2019 ChinhPC <chinhphancong@outlook.com>
 *
 * Author: ChinhPC
 *
 * This file is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * struct rgbled_pixel - the pixel format used by rgbled
 * @red_led: red value
 * @green_led: green value
 * @blue_led: blue value
 */
struct rgbled_pixel {
	unsigned char	red_led;
	unsigned char	green_led;
	unsigned char	blue_led;
};

struct spi_singleled{
	char data[3];
};

struct spiled_strip{
	struct spi_singleled data[12];
};

/**
 * struct rgbled_strip - the rgbled strip format
 */

struct rgbled_strip{
	struct rgbled_pixel 	led1;
	struct rgbled_pixel 	led2;
	struct rgbled_pixel 	led3;
	struct rgbled_pixel 	led4;
};

#define red (struct rgbled_pixel){0xff, 0, 0}

#define green (struct rgbled_pixel){0, 0xff, 0}

#define blue (struct rgbled_pixel){0, 0, 0xff}

#define increase(led, ...) led.##__VAR_ARGS__##++