#include <stdio.h>
#include "ws2812b.h"
#include <linux/string.h>

#define DEBUG
#ifdef DEBUG
void print_rgbled_pixel(struct rgbled_pixel data)
{
	printf("%X\t", data.red_led); printf("%X\t", data.green_led); printf("%X\n\r", data.blue_led);
}

void print_spiled_pixel(struct spi_singleled data)
{
	printf("\n\r%hhX\t", data.data[2]); printf("%hhX\t", data.data[1]); printf("%hhX\n\r\n", data.data[0]);
}
#endif

struct spi_singleled rgb_to_spi_singleled(unsigned char *led)
{
	int i;
	unsigned int tmp = 0;
	struct spi_singleled tmp_spi_singleled = {0, 0, 0};

	for(i = 0; i < 8; i++)
	{
		if((*led >> i) & 1) tmp |= (0x3 << (3 * i));
		else tmp |= (0x1 << (3 * i));
	}

	memcpy(&tmp_spi_singleled, &tmp, 3);

	return tmp_spi_singleled;
}

static int decode_rgb_to_spi(struct rgbled_strip *rgbled_strip, struct spiled_strip *spiled_strip)
{
	int i;
	struct spi_singleled tmp_spi_singleled = {0, 0, 0};

	for(i=0; i<12; i++)
	{
	tmp_spi_singleled = rgb_to_spi_singleled(&(rgbled_strip->led1.red_led) + i);
	memcpy(&(spiled_strip->data[0].data[0]) + i*3,(char*)&tmp_spi_singleled,3);
#ifdef DEBUG
//	printf("%X\r\n", tmp);
	print_spiled_pixel(tmp_spi_singleled);
//	printf("size=%lu\n\r\n", sizeof(unsigned int));
#endif
	}
	return 0;
}

int main()
{
	int ret=1;
	struct rgbled_strip rgbled_strip;
	struct spiled_strip spiled_strip;
	rgbled_strip.led1 = green; rgbled_strip.led2 = green;
	rgbled_strip.led3 = green; rgbled_strip.led4 = green;
	ret = decode_rgb_to_spi(&rgbled_strip,&spiled_strip);
#ifdef DEBUG
	print_rgbled_pixel(rgbled_strip.led1);
	print_spiled_pixel(spiled_strip.data[0]);
	print_spiled_pixel(spiled_strip.data[1]);
	print_spiled_pixel(spiled_strip.data[2]);
#endif
	return ret;
}