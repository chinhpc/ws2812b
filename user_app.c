#include <stdio.h>
#include <fcntl.h> // for open
#include <unistd.h> // for close
#include "ws2812b.h"

int main()
{
	struct rgbled_strip rgbled_strip;
	char buff[36];
	int fd;

	rgbled_strip.led1 = (struct rgbled_pixel){0x00,0x00,0xff}; rgbled_strip.led2 = (struct rgbled_pixel){0x00,0x00,0xff};
	rgbled_strip.led3 = (struct rgbled_pixel){0x00,0x00,0xff}; rgbled_strip.led4 = (struct rgbled_pixel){0x00,0x00,0xff};

	fd = open("/dev/my_dev1", O_RDWR);

	write(fd, &rgbled_strip, sizeof(struct rgbled_strip));
	read(fd, buff, 36);
	close(fd);
	return 0;
}